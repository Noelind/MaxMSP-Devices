function log() {
  for(var i=0,len=arguments.length; i<len; i++) {
    var message = arguments[i];
    if(message && message.toString) {
      var s = message.toString();
      if(s.indexOf("[object ") >= 0) {
        s = JSON.stringify(message);
      }
      post(s);
    }
    else if(message === null) {
      post("<null>");
    }
    else {
      post(message);
    }
  }
  post("\n");
}
 
log("___________________________________________________");
log("Reload:", new Date);

//---------------------------------------------------------//
//--------------------name-listener.js---------------------//

var liveObject = new LiveAPI("this_device");
log("current Max for Live device path:", liveObject.path);

liveObject = new LiveAPI("this_device canonical_parent");
log("current Max for Live device's parent:", liveObject.path);