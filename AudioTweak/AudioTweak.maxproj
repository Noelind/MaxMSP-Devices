{
	"name" : "AudioTweak",
	"version" : 1,
	"creationdate" : 3613208182,
	"modificationdate" : 3613660095,
	"viewrect" : [ 1098.0, 232.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"AudioTweak.amxd" : 			{
				"kind" : "maxforlive",
				"local" : 1,
				"toplevel" : 1
			}
,
			"AudioTweakFilter.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"M4L.ChooserNoelind.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"Browse.DeviceParametersNoelind.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"media" : 		{
			"Gain.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"Filter.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}

		}
,
		"code" : 		{
			"M4L.chooserNoelind.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"name-listener.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 1,
	"amxdtype" : 1633771873,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0
}
